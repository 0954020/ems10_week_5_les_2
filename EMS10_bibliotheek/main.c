#include <msp430.h> 
#include "inc/bibliotheek.h"

/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    // initialiseer de klok op 1 MHz
    // bekijk of false wordt teruggegeven
    if (zet_klok_op_MHz(1) == false)
    {
        while (1); // blijf hier hangen zodat dit wordt opgemerkt met het debuggen
    }

    // push button op P1.0 met interne pull-down
    zet_pin_richting(1, 0, input);
    zet_interne_weerstand(1, 0, pull_down);
    // led op P2.1
    zet_pin_richting(2, 1, output);

    while (1)
    {
        if (input_pin(1, 0))
        {
            output_pin(2, 1, hoog);
        }
        else
        {
            output_pin(2, 1, laag);
        }
    }
}

