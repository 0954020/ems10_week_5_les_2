/*
 * bibliotheek.c
 *
 *  Created on: 14 feb. 2018
 *     Authors: VersD / BroJZ
 */

#include <msp430.h>
#include "../inc/bibliotheek.h"

bool zet_klok_op_MHz(uint8_t mhz)
{
    DCOCTL = 0;

    if (CALBC1_1MHZ==0xFF || CALBC1_8MHZ==0xFF || CALBC1_12MHZ==0xFF || CALBC1_16MHZ==0xFF)
    {
        return false;
    }

    switch (mhz)
    {
    case 1:
        BCSCTL1 = CALBC1_1MHZ; // Set range
        DCOCTL = CALDCO_1MHZ;  // Set DCO step + modulation */
        break;
    case 8:
        BCSCTL1 = CALBC1_8MHZ; // Set range
        DCOCTL = CALDCO_8MHZ;  // Set DCO step + modulation */
        break;
    case 12:
        BCSCTL1 = CALBC1_12MHZ; // Set range
        DCOCTL = CALDCO_12MHZ;  // Set DCO step + modulation */
        break;
    case 16:
        BCSCTL1 = CALBC1_16MHZ; // Set range
        DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */
        break;
    default:
        return false;
    }

    return true;
}

void zet_pin_richting(uint8_t poort, uint8_t pin, Richting richting)
{
    if (poort == 1)
    {
        if (pin <= 7)
        {
            if (richting == input)
            {
                P1DIR &= ~(1<<pin);
            }
            else if (richting == output)
            {
                P1DIR |= 1<<pin;
            }
        }
    }
    else if (poort == 2)
    {
        if (pin <= 5)
        {
            if (richting == input)
            {
                P2DIR &= ~(1<<pin);
            }
            else if (richting == output)
            {
                P2DIR |= 1<<pin;
            }
        }
    }
}

void output_pin(uint8_t poort, uint8_t pin, Waarde waarde)
{
    if (poort == 1){
        if (pin <= 7){
            if (waarde == laag){
                P1OUT &= ~(1 << pin);
            }
            if (waarde == hoog){
                P1OUT |= 1 << pin;
            }
        }
    }
    if ( poort == 2){
        if (pin <= 5){
            if (waarde == laag){
                P2OUT &= ~(1 << pin);
            }
            if (waarde == hoog){
                P2OUT |= 1 << pin;
            }
        }
    }
}

void zet_interne_weerstand(uint8_t poort, uint8_t pin, Weerstand weerstand)
{
    if (poort == 1){
        if (pin <= 7){
            if (weerstand == none){
                P1REN &= ~(1 << pin);
            }
            if (waarde == pull_up){
                P1REN |= 1 << pin;
                P1OUT |= 1 << pin;
            }
            if (waarde == pull_down){
                P1REN |= 1 << pin;
                P1OUT &= ~(1 << pin);
            }
        }
    }
    if ( poort == 2){
        if (pin <= 5){
            if (weerstand == none){
                P2REN &= ~(1 << pin);
            }
            if (waarde == pull_up){
                P2REN |= 1 << pin;
                P2OUT |= 1 << pin;
            }
            if (waarde == pull_down){
                P2REN |= 1 << pin;
                P2OUT &= ~(1 << pin);
            }
        }
    }
}

bool input_pin(uint8_t poort, uint8_t pin)
{
    if (poort == 1){
        if (pin <= 7){
            if (P1IN & (1 << pin)){
                return True;
            }
            else{
                return False;
            }
        }
    }
    if ( poort == 2){
        if (pin <= 5){
            if (waarde == laag){
                P2OUT &= ~(1 << pin);
            }
            if (waarde == hoog){
                P2OUT |= 1 << pin;
            }
        }
    }
}
