#include <msp430.h> 


#pragma vector = TIMER0_A1_VECTOR
__interrupt void Timer_0(){
    static int x = 0;
    static int down = 0;
    if (((x % 75) == 0) && (down == 0) && (TA0CCR1 < 16)){
        TA0CCR1++;
        if (TA0CCR1 == 16){
            down = 1;
        }
    }
    if(((x % 75) == 0) && (down == 1) && (TA0CCR1 > 1)) {
        TA0CCR1--;
        if (TA0CCR1 == 1){
            down = 0;
        }
    }
    TA0CTL &= ~TAIFG;
    x++;
}

/*
#pragma vector=PORT1_VECTOR                     drukknoppen 6.3.3
__interrupt void Port_1(){
    if (TA0CCR1 < 15 && ( P1IFG & 1 << 0)){
        TA0CCR1++;
    }
    if ((TA0CCR1 > 0) && (P1IFG & 1 << 2)){
        TA0CCR1--;
    }
    P1IFG &= ~ (1 << 0 | 1 << 2);
}
*/
/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
    if (CALBC1_1MHZ == 0xFF)
    {
        while(1); // Doe niets
    }
    DCOCTL = 0; // Klokfrequentie = 1 MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
    P1DIR = BIT6;
    P1SEL = BIT6;
    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
    TACTL = TAIE + TASSEL_1 + ID_1 + MC_1; //TAIE voor interrupts
    TA0CCTL1 |= OUTMOD_7;
    BCSCTL3 |= LFXT1S_2;
    __enable_interrupt();
    TA0CCR0 = 15;
    TA0CCR1 = 1;
    /*P1REN |= 1 << 0 | 1 << 2;         Instellingen voor drukknoppen opdr 6.3.3
    P1OUT = 0x00;
    P1IE = 0x05;
    P1IES = 0x05;
    */
    __low_power_mode_1();


 while(1);
    return 0;
}
