#include <msp430.h>

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(){
    static int status = 4;
    if ((P1IFG & 1 << 0) && (status == 4)){
        P2OUT ^= (1 << 2 | 1 << 0);
        status = 1;
        P1IFG &= ~ (1 << 0 | 1 << 1);
    }
    if ((P1IFG & 1 << 0) && (status == 1)){
        P2OUT ^= (1 << 2 | 1 << 0);
        status = 4;
        P1IFG &= ~(1 << 0 | 1 << 1);
    }
    if ((P1IFG & 1 << 1) && (status == 1)){
            P2OUT ^= (1 << 1 | 1 << 2);
            status = 2;
            P1IFG &= ~ (1 << 0 | 1 << 1);
        }
        if ((P1IFG & 1 << 1) && (status == 2)){
            P2OUT ^= (1 << 1 | 1 << 2);
            status = 1;
            P1IFG &= ~(1 << 0 | 1 << 1);
        }
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop de watchdog timer
    if (CALBC1_1MHZ == 0xFF)
    {
        while(1); // Doe niets
    }
    DCOCTL = 0; // Klokfrequentie = 1 MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P1REN |= 1 << 0 | 1 << 2;
    P1OUT = 1 << 1;
    P1IN = 0x05;
    P1IE = 0x05; // 0b00000011
    P1IES = 0x05; // 0b00000010
    P1IFG = 0x00; // 0b00000000
    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
    __enable_interrupt();

    __low_power_mode_4();
    return 0;
}
