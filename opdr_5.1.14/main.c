#include <msp430.h> 

int main(void)
{

    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer

    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
    P1DIR &= ~(1<<0); // zet pin P1.0 op intput
    P1REN |= 1<<0; // zet interne weerstand aan bij pin P1.0
    P1OUT &= ~(1<<0); // selecteer pull down weerstand op pin P1.0.
    int knop_waarde = 0, knop_waarde_oud = 0, knop_teller = 0;

    while (1)
    {
        // welke waarde heeft bit0 nu?
        knop_waarde = (P1IN & (1<<0));

        // als nu de knop ingedrukt is EN vorige keer de knop niet was ingedrukt.
        if ((knop_waarde == 0 && knop_waarde_oud != 0) || (knop_waarde == 1 && knop_waarde_oud == 0))
        {
            knop_teller++; // hoog teller op

            if (knop_teller == 5)
            {
                P2OUT ^= 1<<0; // inverteer de led
                knop_teller = 0; // reset de teller
            }
        }

        knop_waarde_oud = knop_waarde;
        __delay_cycles(15 * 1100); // ongeveer 20ms wachten
    }

    return 0;
}
