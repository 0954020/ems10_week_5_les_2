#include <msp430.h>

/*Motor aansturing
 * Voor de motoraansturing maken we gebruik van het P1OUT register dat de L298M aan zal sturen.
 * Bij P1OUT = 1 << 0 zal de linkermotor gaan rijden
 * Bij P1OUT = 1 << 1 zal de rechtermotor gaan rijden
 * bij P1OUT = 1 << 2 zal de linkermotor gaan rijden terwijl de rechtermotor achteruit draait(keren op zijn plaats)
 * onderstaande code is verantwoordelijk voor het volgen van een route en aansturen van de motoren in de juiste situatie
 */

void vooruit(){                                         //functie verantwoordelijk voor het vooruit rijden van de robot
    while(1){
        TACTL = TASSEL_1 + ID_1 + MC_1;                      //
        TA0CCTL1 |= OUTMOD_7;                                //reset set output mode
        BCSCTL3 |= LFXT1S_2;                                 //selectie van de VLOCLK
        TA0CCR0 = 100;                                       //TA0CCR0 en CCR1 maken samen duty cycle van cycle / 100
        TA0CCR1 = 50;                                        //TACCR1 kan outputten op pin 1.2 en 1.6 CCR2 op 2.4 en 2.5
        TA0CCR1 = 50;                                        //pwm van 50 % op beide motoren voor vooruit
    }

}

void links(){                                   //functie voor links draaien(bij een kruispunt) voor linker motor op pin 1.2 of pin 1.6
    TACTL = TASSEL_1 + ID_1 + MC_1;                      //
    TA0CCTL1 |= OUTMOD_7;                                //reset set output mode
    BCSCTL3 |= LFXT1S_2;                                 //selectie van de VLOCLK
    TA0CCR0 = 100;                                       //TA0CCR0 en CCR1 maken samen duty cycle van cycle / 100
    TA0CCR1 = 50;                                        //TACCR1 kan outputten op pin 1.2 en 1.6
    while (1){

    }
}

void rechts(){                                  //functie voor rechts draaien(bij een kruispunt) voor rechter motor op pin 2.5 en 2.4
    TACTL = TASSEL_1 + ID_1 + MC_1;                      //
    TA0CCTL1 |= OUTMOD_7;                                //reset set output mode
    BCSCTL3 |= LFXT1S_2;                                 //selectie van de VLOCLK
    TA0CCR0 = 100;                                       //TA0CCR0 en CCR1 maken samen duty cycle van cycle / 100
    TA0CCR2 = 50;                                     //TACCR1 kan outputten op pin 1.2 en 1.6

}
/*
void omkeren(){                                         //functie voor het omkeren
    P1OUT |= 1 << 2;
    interrupt(1000);
    P1OUT &= 0b11111011
}

void linksPWM(int cycle){                                //functie voor links bijsturen tijdens rechtdoor rijden
    TACTL = TASSEL_1 + ID_1 + MC_1;                      //
    TA0CCTL1 |= OUTMOD_7;                                //reset set output mode
    BCSCTL3 |= LFXT1S_2;                                 //selectie van de VLOCLK
    TA0CCR0 = 100;                                       //TA0CCR0 en CCR1 maken samen duty cycle van cycle / 100
    TA0CCR1 = cycle;                                     //TACCR1 kan outputten op pin 1.2 en 1.6

}

void rechtsPWM(int x = berekenPWM()){                   //functie voor rechts bijsturen tijdens rechtdoor rijden
    P1OUT |= 1 << 1;
    interrupt(25 * (180 - x));
    P1OUT |= 1 << 0;
    interrupt(25 * (20 + x));
    P1OUT &= 0b11111100;
}

void rijden(char a){                     //functie die de motor aanstuurt d.m.v een character uit de route die de richting aangeeft
    switch (a){
    case 'D':
        vooruit();
        break;
    case 'L':
        links();
        break;
    case 'R':
        rechts();
        break;
    case 'W':
        omkeren();
    default:

    }
}

void PWM(char a, int cycle){                             //functie die een pwm signaal genereert
    TACTL = TASSEL_1 + ID_1 + MC_1;                      //
    TA0CCTL1 |= OUTMOD_7;                                //reset set output mode
    BCSCTL3 |= LFXT1S_2;                                 //selectie van de VLOCLK
    TA0CCR0 = 100;                                       //TA0CCR0 en CCR1 maken samen duty cycle van cycle / 100
    TA0CCR1 = cycle;                                     //TACCR1 kan outputten op pin 1.2 en 1.6
}
*
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	P1DIR = 0x07;
	P1OUT = 0x00;
	P1DIR |= BIT6;
	P1SEL = BIT6;
	__enable_interrupt();

    __low_power_mode_1();
	
	return 0;
}
