#include <msp430.h>
#include <stdint.h>

int main()
{
    WDTCTL = WDTPW | WDTHOLD; // Stop de watchdog timer

    // Eerst alle code die 1 keer uitgevoerd moet worden
    // Zoals bijvoorbeeld het instellen van de I/O
    P1OUT = 0x08;
    P1DIR = 0x03;
    P1REN = 0x08;

    // en het declareren van variabelen
    volatile uint16_t wachttijd;
    int it = 0;
    while (1){
        while ((P1IN & 1 << 3) == 0)
        {
            /* code voor een counter die het ledje aanzet terwijl er getelt wordt met een voorwaarde
             dat als er een hoog signaal is op de 4de pin de led uit gaat
             Hier de rest van het programma
             dat continue uitgevoerd moet worden*/
            P1OUT |= (it & 0b00000011);
            P1OUT &= (it | 0b11111100);

            if (it % 3 == 0){
                P1OUT ^= 0b00000100;
            }
            /*while ((P1IN & isolate) == 1<<3){
                P1OUT = 0;
            }*/
            wachttijd = 60000;
            while (wachttijd) {
                wachttijd--;
            }
            it++;
        }
    }
}
