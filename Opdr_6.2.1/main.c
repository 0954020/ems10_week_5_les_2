#include <msp430.h> 

/*
#pragma vector = TIMER0_A1_VECTOR
__interrupt void Timer_0(){
    P2OUT ^= BIT0;
    TA0CTL &= ~TAIFG;
}
*/
/**
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
    if (CALBC1_1MHZ == 0xFF)
    {
        while(1); // Doe niets
    }
    DCOCTL = 0; // Klokfrequentie = 1 MHz
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
    P1DIR = 0x02;
    P1SEL = 0x02;
	P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
	P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
	TACTL = TASSEL_1 + ID_0 + MC_1;
	TA0CCTL0 |= OUTMOD_4;
	BCSCTL3 |= LFXT1S_2;
	//__enable_interrupt();
	TA0CCR0 = 16 - 1;
	__low_power_mode_1();


 while(1);
	return 0;
}
