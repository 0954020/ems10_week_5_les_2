#include <msp430.h> 

void zet_led_aan(int lednummer)
{
    P2OUT = P2OUT | 1 << (lednummer - 1);
}

void zet_led_uit(int lednummer){
      P2OUT = P2OUT ^ 1 << (lednummer - 1);
}
int knop_is_ingedrukt(){
    if ((P1IN & 1<<0) == 1){
        return 1;
    }
    return 0;
}
int zet_klok_op_MHz(int mhz){
    switch (mhz){
    case 1:
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;
        return 1;
    case 8:
        BCSCTL1 = CALBC1_8MHZ;
        DCOCTL = CALDCO_8MHZ;
        return 1;
    case 12:
        BCSCTL1 = CALBC1_12MHZ;
        DCOCTL = CALDCO_12MHZ;
        return 1;
    case 16:
        BCSCTL1 = CALBC1_16MHZ;
        DCOCTL = CALDCO_16MHZ;
        return 1;
    }
    return 0;
}
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop de watchdog timer
    P2DIR |= 1<<2 | 1<<1 | 1<<0; // zet pins P2.2, P2.1 en P2.0 op output
    P2OUT &= ~(1<<2 | 1<<1 | 1<<0); // maak pins P2.2, P2.1 en P2.0 laag
    P1DIR = 0x00;
    P1REN = 0x01;
    P1OUT = 0x00;
    if (CALBC1_16MHZ==0xFF)                 // If calibration constant erased
      {
            while(1);                               // do not load, trap CPU!!
      }
    DCOCTL = 0;                               // Select lowest DCOx and MODx settings
    BCSCTL1 = CALBC1_16MHZ;                   // Set range
    DCOCTL = CALDCO_16MHZ;                    // Set DCO step + modulation*/
    zet_klok_op_MHz(16);
    while (1)
        {
            while (knop_is_ingedrukt())
            {
                __delay_cycles(500000); // wacht ongeveer een seconde
                zet_led_uit(3);
                zet_led_aan(1);
                __delay_cycles(500000); // wacht ongeveer een seconde
                zet_led_uit(1);
                zet_led_aan(2);
                __delay_cycles(500000); // wacht ongeveer een seconde
                zet_led_uit(2);
                zet_led_aan(3);
            }
        }

    return 0;
}
